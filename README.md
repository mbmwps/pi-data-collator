
# USAGE: collect data from PI model on and format it into an excel workbook.
terminal commands may vary by OS
## 2 directories will be created 
raw_data. For storing the json that comes from renew

excel. For storing the for created workbooks

## Create a new python virtual environment:
terminal: python -m venv VENV

## Start the environment
windows terminal: VENV/Scripts/Activate.ps1

linux terminal: source VENV/Scripts/activate

## Install requirements
terminal: pip install -r requirements.txt

## create a .env file
HX_API_USER='insert API user ID'

HX_API_KEY='insert API user KEY'

## run the script 
terminal: python to_excel.py


