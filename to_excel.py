import pandas as pd 
import requests
import os
from decouple import config
from datetime import datetime as dt
import json 


def policy_collector(model_id, schema_parameter_list):
    user = config("HX_API_USER")
    password = config("HX_API_KEY")

    # get all policy ids relating to a model id
    url =f"https://api.optio.hxrenew.com/api/v1/policies/options/"


    # Unpaged True to get all policies
    params = {
        "model_id": model_id,
        "unpaged":True
    }

    # Call to API to get policy IDs
    try:
        response = requests.get(url, params=params, auth=(user, password))
    except requests.RequestException:
        print("Unable to connect to API")

    # Error handling based on status code returned by API
    if response.ok:
        # results stored in dict : {data:{variable_name: value}}
        result = response.json()
        policy_ids = []
        for policy in result:
            policy_ids.append(policy["id"])

    list_of_policy_jsons = []

    # For each policy id get its snapshot
    for i, policy_id in enumerate(policy_ids,1):
        if i == 1:
            print(f"collecting policy {i} of {len(policy_ids)} from renew.")
        if i % 10 == 0:
            print(f"collecting policy {i} of {len(policy_ids)} from renew.")

        # URL & parameters for the API
        url = f"https://api.optio.hxrenew.com/api/v1/policies/options/{policy_id}/snapshot"
        params = {
            "path": schema_parameter_list
        }

        # Call to API
        try:
            response = requests.get(url, params=params, auth=(user, password))
        except requests.RequestException:
            print("Unable to connect to API")

        # Error handling based on status code returned by API
        if response.ok:
            # results stored in dict : {data:{variable_name: value}}
            result = response.json()
            list_of_policy_jsons.append(result)

    # Save the raw data
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    dir_path = os.path.join(parent_dir, "raw_data")
    os.makedirs(dir_path, exist_ok=True)
    file_path = os.path.join(dir_path, f"pi_data_{now}.json")

    with open(file_path, 'w') as f:
        json.dump(list_of_policy_jsons, f)

    return list_of_policy_jsons



def pi_to_excel():

    data = policy_collector(8, [
                "/programme",
                "/policy",
                "/pricing",
                "/risk_assessment",
                "/hx_core",
            ])


    policies_data = []
    programme_data = []
    location_exposure_data = []
    profession_exposure_data = []


    for i,record in enumerate(data, 1):
        if i == 1:
            print(f"extracting policy {i} of {len(data)}")
        if i % 10 == 0:
            print(f"extracting policy {i} of {len(data)}")


        pol = record["data"]
        try:
            policy_data = {
                #Core
                "id": record["id"],
                "inception_date": pol["hx_core"]["inception_date"],
                "expiry_date": pol["hx_core"]["expiry_date"],

                # Policy
                "status" : pol["policy"]["status"],
                "insured_name" : pol["policy"]["insured_name"],
                "retroactive_inception_date" : pol["policy"]["retroactive_inception_date"],
                "domicile" : pol["policy"]["domicile"],
                "broker" : pol["policy"]["broker"],
                "underwriter_name" : pol["policy"]["underwriter_name"],
                "quoting_currency" : pol["policy"]["quoting_currency"],
                "fx_rate" : pol["policy"]["fx_rate"],
                "revenue_premccy" : pol["policy"]["revenue_premccy"],
                "exposure_base_value" : pol["policy"]["exposure_base_value"],
                "rateable_revenue_gbp" : pol["policy"]["rateable_revenue_gbp"],
                "costs_handling" : pol["policy"]["costs_handling"],


                # pricing
                "base_premium": pol["pricing"]["base_premium"],
                "base_rate": pol["pricing"]["base_rate"],

                # risk assessment
                "operational_and_risk_control": pol["risk_assessment"]["operational_and_risk_control"]["selected"],
                "operational_and_risk_control_comment": pol["risk_assessment"]["operational_and_risk_control"]["comment"],

                "mix_of_clients": pol["risk_assessment"]["mix_of_clients"]["selected"],
                "mix_of_clients_comment": pol["risk_assessment"]["mix_of_clients"]["comment"],

                "letters_of_engagement": pol["risk_assessment"]["letters_of_engagement"]["selected"],
                "letters_of_engagement_comment": pol["risk_assessment"]["letters_of_engagement"]["comment"],

                "financial_rating": pol["risk_assessment"]["financial_rating"]["selected"],
                "financial_rating_comment": pol["risk_assessment"]["financial_rating"]["comment"],

                "limit_primary": pol["risk_assessment"]["limit_primary"]["selected"],
                "limit_primary_comment": pol["risk_assessment"]["limit_primary"]["comment"],

                "limit_excess": pol["risk_assessment"]["limit_excess"]["selected"],
                "limit_excess_comment": pol["risk_assessment"]["limit_excess"]["comment"],

                "compliance": pol["risk_assessment"]["compliance"]["selected"],
                "compliance_comment": pol["risk_assessment"]["compliance"]["comment"],

                "claims_experience_primary": pol["risk_assessment"]["claims_experience_primary"]["selected"],
                "claims_experience_primary_comment": pol["risk_assessment"]["claims_experience_primary"]["comment"],

                "claims_experience_excess": pol["risk_assessment"]["claims_experience_excess"]["selected"],
                "claims_experience_excess_comment": pol["risk_assessment"]["claims_experience_excess"]["comment"],

                "terms_and_conditions": pol["risk_assessment"]["terms_and_conditions"]["selected"],
                "terms_and_conditions_comment": pol["risk_assessment"]["terms_and_conditions"]["comment"],

                "other_risks": pol["risk_assessment"]["other_risks"]["selected"],
                "other_risks_comment": pol["risk_assessment"]["other_risks"]["comment"],
            
            }

            
            # Programme Data
            for layer in pol["programme"]["layers"]:
                pol_layer = {
                    "id": record["id"],
                    "policy_reference": layer["policy_reference"],
                    "expiring_policy_reference": layer["expiring_policy_reference"],
                    "limit": layer["limit"],
                    "attachment": layer["attachment"],
                    "share": layer["share"],
                    "sir": layer["sir"],
                    "brokerage": layer["brokerage"],
                    "mechanical_gross_premium": layer["mechanical_gross_premium"],
                    "technical_gross_premium": layer["technical_gross_premium"],
                    "agreed_gross_premium": layer["agreed_gross_premium"],
                    "priced_loss_ratio": layer["priced_loss_ratio"],
                    "return_on_capital": layer["return_on_capital"],
                    "adequacy_to_mechanical_premium": layer["adequacy_to_mechanical_premium"],
                    "adequacy_to_technical_premium": layer["adequacy_to_technical_premium"],
                    "loss_cost": layer["loss_cost"],
                    "rarc_total" : layer["rarc"]["total"],
                    "rarc_cash" : layer["rarc"]["pct_change"]["cash"],
                    "rarc_lda" : layer["rarc"]["pct_change"]["lda"],
                    "rarc_coverage" : layer["rarc"]["pct_change"]["coverage"],
                    "other": layer["rarc"]["pct_change"]["other"],
                }
                programme_data.append(pol_layer)

            # Location Exposure Data
            # Add 1 record of domicil;e
            location_exposure_data.append({              
                        "id": record["id"],
                        "location": pol["pricing"]["exposure"]["location"]["domicile"]["region"],
                        "percent": pol["pricing"]["exposure"]["location"]["domicile"]["pct"],
                        })

            # All worldwide records
            for region in pol["pricing"]["exposure"]["location"]["worldwide"]["regions"]:
                if region["region"] is not None:
                    location_exposure_data.append({
                        "id": record["id"],
                        "location": region["region"],
                        "percent": region["pct"],
                    }) 
                
            # All americnan state records    
            for region in pol["pricing"]["exposure"]["location"]["usa"]["regions"]:
                if region["region"] is not None:
                    location_exposure_data.append({
                        "id": record["id"],
                        "location": region["region"],
                        "percent": region["pct"],
                    })
            
            # Profession Exposure Data
            for profession_name, profession in pol["pricing"]["exposure"]["profession"].items():
                if profession["is_selected"]:
                    for aop in profession["areas_of_practice"]:
                        profession_exposure_data.append({
                            "id": record["id"],
                            "area_of_practice": aop["area_of_practice"],
                            "percent": aop["pct"]
                        })

            policies_data.append(policy_data)
        except TypeError as e:
            id = record["id"]
            print(f"{e}: {id}")


    pol_df = pd.DataFrame(policies_data)
    prog_df = pd.DataFrame(programme_data)
    location_df = pd.DataFrame(location_exposure_data)
    profession_df = pd.DataFrame(profession_exposure_data)
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")

    parent_dir = os.path.dirname(os.path.abspath(__file__))
    dir_path = os.path.join(parent_dir, "excel")
    os.makedirs(dir_path, exist_ok=True)
    file_path = os.path.join(dir_path, f"pi_data_{now}.xlsx")

    with pd.ExcelWriter(file_path) as writer:
        pol_df.to_excel(writer, sheet_name="policy", index=False)
        prog_df.to_excel(writer, sheet_name="programme", index=False)
        location_df.to_excel(writer, sheet_name="location", index=False)
        profession_df.to_excel(writer, sheet_name="profession", index=False)



if __name__ == "__main__":
    pi_to_excel()

    

